import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AgmCoreModule } from '@agm/core';

import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';

import {MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef,
  DocumentRef, MapServiceFactory, BingMapAPILoaderConfig, BingMapAPILoader,
  GoogleMapAPILoader, GoogleMapAPILoaderConfig, BingMapServiceFactory } from 'angular-maps';

import { AppComponent } from './app.component';

import { MainpanelModule } from './layouts/mainpanel/mainpanel.module';
import { SidePanelModule } from './layouts/sidepanel/sidepanel.module';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SidePanelModule,
    MainpanelModule,
    BrowserAnimationsModule,
    MapModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDUvPw7dLulAt6ZEEnhIAirZZGtDQeuhjo'
    }),
    MatGoogleMapsAutocompleteModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
