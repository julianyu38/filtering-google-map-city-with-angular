import { Component, OnInit } from '@angular/core';

import { MapService } from './../../services/map.service';
import example from '../../../assets/example.json';
@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.component.html',
  styleUrls: ['./googlemap.component.scss']
})
export class GooglemapComponent implements OnInit {

  texto: string = 'Germany Braz - Dortumund Filtering';
  lat: number = -23.8779431;
  lng: number = -49.8046873;
  zoom: number = 10;

  datas: any = example.datas;

  constructor(private _mapservice: MapService) {

  }

  ngOnInit() {
  }

}
