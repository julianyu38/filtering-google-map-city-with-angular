import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AgmCoreModule } from '@agm/core';

import { MapFilterPipe } from 'src/app/pipes/filter.pipe';

import { GooglemapComponent } from './googlemap.component';

@NgModule({
  declarations: [
    GooglemapComponent,
    MapFilterPipe
  ],
  imports: [
    BrowserModule,
    AgmCoreModule
  ],
  exports: [
    GooglemapComponent
  ],
  providers:[]
})
export class GooglemapModule { }
