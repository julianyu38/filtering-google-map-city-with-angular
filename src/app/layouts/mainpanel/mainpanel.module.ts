import { NgModule } from '@angular/core';

import { GooglemapModule } from './../../components/googlemap/googlemap.module';

import { MainpanelComponent } from './mainpanel.component';

@NgModule({
  declarations: [
    MainpanelComponent
  ],
  imports: [
    GooglemapModule
  ],
  exports: [
    MainpanelComponent
  ],
  providers:[]
})

export class MainpanelModule { }
