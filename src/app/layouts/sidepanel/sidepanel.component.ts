import { Component, OnInit, ElementRef, ViewChild, EventEmitter, Input, Output, NgZone } from '@angular/core';

import { Location, Appearance } from '@angular-material-extensions/google-maps-autocomplete';

import { Options, LabelType, ChangeContext } from 'ng5-slider';

import PlaceResult = google.maps.places.PlaceResult;

import { MapService } from './../../services/map.service';
import example from '../../../assets/example.json';

@Component({
  selector: 'app-sidepanel',
  templateUrl: './sidepanel.component.html',
  styleUrls: ['./sidepanel.component.scss']
})


export class SidepanelComponent implements OnInit {

  postal_code: any;

  main_lat: string;
  main_lng: string;

  low_kilo: number;
  high_kilo: number;

  @ViewChild('bike_div') div_bike: ElementRef;
  @ViewChild('car_div') div_car: ElementRef;
  @ViewChild('truck_div') div_truck: ElementRef;
  @ViewChild('distance_slider') slider_distance: ElementRef;

  div_bike_clicked: boolean = false;
  div_car_clicked: boolean = false;
  div_truck_clicked: boolean = false;

  public appearance = Appearance;
  public zoom: number;

  public latitude: number;
  public longitude: number;
  public selectedAddress: PlaceResult;

  onAutocompleteSelected(result: PlaceResult) {
    console.log('onAutocompleteSelected: ', result);
  }

  onLocationSelected(location: Location) {
    console.log('onLocationSelected: ', location);
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }

  sliderMinValue: number = 1;
  sliderMaxValue: number = 10;
  sliderValue: number = (this.sliderMaxValue - this.sliderMinValue) / 2;
  sliderOptions: Options = {
    floor: 0,
    ceil: 2000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return value + 'Km';
        case LabelType.High:
          return value + 'Km';
        default:
          return value + 'Km';
      }
    }
  };

  opening_days = [
    { label: 'Monday', checked: false },
    { label: 'Tuesday', checked: false },
    { label: 'Wendesday', checked: false },
    { label: 'Thursday', checked: false },
    { label: 'Friday', checked: false },
    { label: 'Saturday', checked: false },
    { label: 'Sunday', checked: false }
  ];

  theater_days = [
    { label: 'Monday', checked: false },
    { label: 'Tuesday', checked: false },
    { label: 'Wendesday', checked: false },
    { label: 'Thursday', checked: false },
    { label: 'Friday', checked: false },
    { label: 'Saturday', checked: false },
    { label: 'Sunday', checked: false }
  ];



  onOpen_Change(event, index, item) {
    this._mapservice.openDays[item.label] = !this._mapservice.openDays[item.label];
  }

  onTheatre_Change(event, index, item) {
    this._mapservice.theDays[item.label] = !this._mapservice.theDays[item.label];
  }

  constructor(private _mapservice: MapService, private zone: NgZone) {

  }

  ngOnInit() {
  }

  bike_click() {
    this.div_bike_clicked = !this.div_bike_clicked;
    this._mapservice.bike_status = this.div_bike_clicked;
    if (this.div_bike_clicked) {
      this.div_bike.nativeElement.setAttribute('style', 'background-color: #b7e2f0; border:2px solid #0db9f0;');
    } else {
      this.div_bike.nativeElement.setAttribute('style', 'background-color: #ffffff; border:2px solid #c8c8c8;');
    }
    this.filter()
  }

  car_click() {
    this.div_car_clicked = !this.div_car_clicked;
    this._mapservice.car_status = this.div_car_clicked;
    if (this.div_car_clicked) {
      this.div_car.nativeElement.setAttribute('style', 'background-color: #b7e2f0; border:2px solid #0db9f0;');
    } else {
      this.div_car.nativeElement.setAttribute('style', 'background-color: #ffffff; border:2px solid #c8c8c8;');
    }
    this.filter()
  }

  truck_click() {
    this.div_truck_clicked = !this.div_truck_clicked;
    this._mapservice.truck_status = this.div_truck_clicked;
    if (this.div_truck_clicked) {
      this.div_truck.nativeElement.setAttribute('style', 'background-color: #b7e2f0; border:2px solid #0db9f0;');
    } else {
      this.div_truck.nativeElement.setAttribute('style', 'background-color: #ffffff; border:2px solid #c8c8c8;');
    }
    this.filter()

  }

  onUserChangeStart(changeContext: ChangeContext): void {
    this._mapservice.distance = changeContext.value;
    this.filter()
  }

  onUserChange(changeContext: ChangeContext): void {
    this._mapservice.distance = changeContext.value;

    this.filter()
  }

  onUserChangeEnd(changeContext: ChangeContext) {
    this._mapservice.distance = changeContext.value;
    this.filter()
  }


  @ViewChild('addresstext') addresstext: any;
  // @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @Input() adressType: string = 'geocode';

  ngAfterViewInit() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
        componentRestrictions: { country: 'DE' },
        types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
    });
  }

  invokeEvent(place: any) {
    // console.log(place);
    // this.setAddress.emit(place);
    var address_components = place.address_components;
    console.log(place);
    // this.main_lat = place.geometry.location.lat();
    // this.main_lng = place.geometry.location.lng();
    this._mapservice.map_main_lat = place.geometry.location.lat();
    this._mapservice.map_main_lng = place.geometry.location.lng();
    this.zone.run(() => {});
    return;
  }

  filter() {

  }

}
