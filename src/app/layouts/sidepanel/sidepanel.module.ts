import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';

import { AgmCoreModule } from '@agm/core';

import { Ng5SliderModule } from 'ng5-slider';

import { TypeaheadModule } from 'ngx-foundation';

import { MapService } from './../../services/map.service';

import { SidepanelComponent } from './sidepanel.component';
@NgModule({
  declarations: [
    SidepanelComponent
  ],
  imports: [

    CommonModule,
    FormsModule,
    MatIconModule,
    MatCheckboxModule,
    MatSliderModule,
    Ng5SliderModule,
    TypeaheadModule.forRoot(),
    MatGoogleMapsAutocompleteModule
  ],
  exports: [
    SidepanelComponent
  ],
  providers: [MapService],
})
export class SidePanelModule { }
