import {Pipe, PipeTransform} from '@angular/core';

import { MapService } from './../services/map.service';
@Pipe ({
  name: 'mapfilter',
  pure: false
})
export class MapFilterPipe implements PipeTransform  {
  constructor(
    public mapService: MapService) { }

  transform(items: any[]): any {
    var newValues = items.filter((item => {
      if (!this.arePointsNear(
            item.companyAddress,
        {
          lat: this.mapService.map_main_lat,
          lng: this.mapService.map_main_lng
        },
        this.mapService.distance))
        return null;

      //Driving filter
      if (this.mapService.bike_status && item.services.classes.data.bike.length == 0)
        return null;
      if (this.mapService.car_status && item.services.classes.data.car.length == 0)
        return null;
      if (this.mapService.truck_status && item.services.classes.data.truck.length == 0)
        return null;

      //Time filter
      const { openDays, theDays } = this.mapService;
      let isOpenOk = true;
      Object.keys(openDays).forEach(key => {
        if (!openDays[key] || !isOpenOk) return;

        key = key.toLowerCase();
        if (!item.services.opening_times[key])
          isOpenOk = false;
      })
      if (!isOpenOk) return null;

      let isTheOk = true;
      Object.keys(theDays).forEach(key => {
        if (!theDays[key] || !isTheOk) return;

        key = key.toLowerCase();
        if (!item.services.theory_times[key])
          isTheOk = false;
      })
      if (!isTheOk) return null;

      return item;
    }));
    return newValues;
  }

  arePointsNear(checkPoint, centerPoint, km) {
    var ky = 40000 / 360;
    var kx = Math.cos(Math.PI * centerPoint.lat / 180.0) * ky;
    var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
    var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
    return Math.sqrt(dx * dx + dy * dy) <= km;
  }

  isDrivingLicense(drivingLicnes) {

  }
}
