import { Injectable } from '@angular/core';

@Injectable()
export class MapService {
  constructor() { }
  map_main_lat: number = 51.514244;
  map_main_lng: number = 7.468429;
  openDays = {};
  theDays = {};


  distance: number = 5;

  bike_status: boolean = false;
  car_status: boolean = false;
  truck_status: boolean = false;

  zoom() {
    return parseInt(Math.log2(40000 / this.distance * 2).toString()) - 1;
  }
}
